function updatePrice() {
	let type = document.getElementsByName("type")[0];
	let price = 0;
	let prices = getPrices();
	let index = parseInt(type.value) - 1;
	price = (index >= 0 ? prices.types[index] : price);

	let radio = document.getElementById("radio");
	radio.style.display = (type.value == "2" ? "block" : "none");

	let radios = document.getElementsByName("option");
	radios.forEach(function(option) {
		if (option.checked) {
			let optionPrice = prices.options[option.value];
			price += (optionPrice !== undefined ? optionPrice : 0); 
		}
	});


	let check = document.getElementById("checkbox");
	check.style.display = (type.value == "3" ? "block" : "none");

	let checkboxes = document.querySelectorAll("#checkbox input");
	checkboxes.forEach(function(property) {
		if (property.checked) {
			let propPrice = prices.properties[property.value];
			price += (propPrice !== undefined ? propPrice : 0); 
		}
	});

	let result = document.getElementById("result");
	let count = document.getElementById("count");
	result.innerHTML = (count.value != "" && price > 0 ? '<p class="alert alert-success">C вас ' + parseInt(count.value) * price + " рублей</p>" : '<p class="alert alert-warning">Введите количество и выберите параметры</p>');
}

function getPrices() {
	return {
    	types: [1000, 2000, 1500],
    	options: {
   			option1: 250,
    		option2: 1250,
    		option3: 2250,
    	},
    	properties: {
    		property1: 500,
    		property2: 750,
    		property3: 1000,
    	}
	};
}

window.addEventListener('DOMContentLoaded', function(event) {
	let radio = document.getElementById("radio");
	radio.style.display = "none";

	let type = document.getElementsByName("type")[0];
	type.addEventListener("change", updatePrice);

	let radios = document.getElementsByName("option");
	radios.forEach(function(option) {
		option.addEventListener("change", updatePrice);
	});

	let checkboxes = document.querySelectorAll("#checkbox input");
	checkboxes.forEach(function(property) {
		property.addEventListener("change", updatePrice);
	});

	updatePrice();
});